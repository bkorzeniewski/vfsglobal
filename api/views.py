from rest_framework import viewsets, mixins
from core.models import Account
from .serializers import AccountSerializer
from rest_framework.decorators import action
from rest_framework.response import Response


class AccountViewSet(viewsets.ModelViewSet):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

    @action(detail=True, methods='PUT')
    def mail_confirm(self, request, *args, **kwargs):
        snippet = self.get_object()
        snippet.is_mail_confirmed = True
        snippet.save()
        return Response(snippet.is_mail_confirmed)