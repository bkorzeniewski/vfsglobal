from rest_framework import serializers
from core.models import Account

class AccountSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Account
        fields = ( 'first_name', 'last_name', 'email_id', 'contact_no', 'password', 'is_mail_confirmed', 'is_active' )