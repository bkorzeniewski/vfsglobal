from django.utils.translation import ugettext_lazy as _
from jet.dashboard import modules
from jet.dashboard.dashboard import Dashboard, AppIndexDashboard


from jet.dashboard.modules import DashboardModule


class TestModule(DashboardModule):
    title = 'Test Module'
    template = 'admin/dashboard/blank.html'
    sub_title = None

    def init_with_context(self, context):
        self.sub_title = 'Sub title'
        pass
class CustomIndexDashboard(Dashboard):
    columns = 3

    def init_with_context(self, context):
        self.available_children.append(TestModule)
        self.children.append(TestModule('Test Module', sub_title='Sub Title'))
