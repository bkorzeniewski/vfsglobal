from django.db import models
from django.contrib.auth.models import User
from . import choices, helpers, validators


class Account(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email_id = models.EmailField()
    contact_no = models.CharField(max_length=15)
    password = models.CharField(max_length=15)
    is_mail_confirmed = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    last_login = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @staticmethod
    def get_available():
        accounts = Account.objects.all()
        for account in accounts:
            if account.applicants.count() < 5:
                return account
        return None

    def __str__(self):
        return self.email_id


class Appointment(models.Model):
    location_id = models.CharField(max_length=3, choices=choices.location_id)
    visa_category_id = models.CharField(max_length=4, choices=choices.visa_category_id)
    expiry_date = models.DateField(validators=[validators.validate_expiry_date])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, related_name='appointments', on_delete=models.CASCADE)

    @property
    def location_name(self):
        return  dict(choices.location_id)[self.location_id]

    @property
    def visa_category_name(self):
        return dict(choices.visa_category_id)[self.visa_category_id]

    def __str__(self):
        return str(self.expiry_date)


class Applicant(models.Model):
    passport_number = models.CharField(max_length=15, unique=True, validators=[validators.alphanumeric])
    date_of_birth = models.DateField(validators=[validators.validate_date_of_birth])
    passport_expiry_date = models.DateField(validators=[validators.validate_passport_expiry_date])
    nationality_id = models.CharField(max_length=3, choices=choices.nationality_id)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    gender_id = models.CharField(max_length=1, choices=choices.gender_id)
    dial_code = models.IntegerField()
    phone_mobile = models.CharField(max_length=15)
    email_id = models.EmailField()
    app_edit_link = models.TextField(null=True, blank=True)
    app_delete_id = models.CharField(max_length=200, null=True, blank=True)
    is_active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    ref_number = models.CharField(max_length=200, null=True, blank=True)
    appointment = models.ForeignKey(Appointment, related_name='applicants', on_delete=models.CASCADE)
    account = models.ForeignKey(Account, related_name='applicants', on_delete=models.CASCADE)


    def __str__(self):
        return self.last_name
