

class Applicant {
    constructor() {
        this.endpoint_scaner = 'http://vps631150.ovh.net/upload';

        this.document = $(document);
        this.modal = $('#image-upload-modal');
        this.fields = [
            'passport_number', 'nationality_id', 'date_of_birth', 'passport_expiry_date', 'first_name',
            'last_name', 'gender_id', 'dial_code', 'phone_mobile', 'email_id'
        ];
        this.formatDate = 'd/m/Y';
        this.imgPreview = $('.img-preview');
        this.previewWrapper = $('#preview-wrapper');
        this.scanningImage = $('.scanning-image');
        this._initFields();
        this._initDaterPicker();
        this._listener();
    }

    _initFields() {
        this.fields.forEach((item) => {
            Object.defineProperty(this, item, {
                get: () => {
                    return $(`#id_${item}`)
                }
            });
        })
    }

    _initDaterPicker() {
        let d = new Date();
        let maxDate = d;
        maxDate.setDate(d.getDate() - 1);
        this.date_of_birth_picker = this.date_of_birth.flatpickr({dateFormat: this.formatDate, maxDate: maxDate});

        let minDate = d;
        minDate.setDate(d.getDate() + 2);
        this.passport_expiry_date_picker = this.passport_expiry_date.flatpickr({
            dateFormat: this.formatDate,
            minDate: minDate
        });

    }


    _listener() {
        this.document.on('change', ':file', e => this._selectedImage(e));
        this.modal.on('shown.bs.modal', () => this._modalOpened());
        this.modal.on('hidden.bs.modal', () => this._modalClosed());
    }

    _modalOpened() {
        let width = this.previewWrapper.width() + 'px';
        this.imgPreview.css('width', width);
        let imgHeight = this.imgPreview.first().height() + 'px';
        this.previewWrapper.css({height: imgHeight, opacity: '1'});
        this.scanningImage.css({width: width, height: imgHeight});
    }

    _modalClosed() {
        this.imgPreview.attr('src', '').css('width', '');
        this.previewWrapper.css({height: '', opacity: '0'});
        this.scanningImage.css({width: '', height: ''});
    }

    _selectedImage(e) {
        let files = e.target.files;
        let file = files && files[0] ? files[0] : null;
        this.loadPreview(file);

        this.scanning(file).then(data => this.handlerScanning(data)).catch(err => {
        });
    }

    loadPreview(file) {
        this.modal.modal('show');
        let reader = new FileReader();

        reader.onload = (e) => {
            this.imgPreview.attr('src', e.target.result);
            //img.src = e.target.result;

        };

        reader.readAsDataURL(file);
    }

    _rezizeImage() {
        let img = new Image();
        img.onload = () => {
            let canvas = document.createElement("canvas");
            let ctx = canvas.getContext("2d");
            let MAX_WIDTH = 1280, MAX_HEIGHT = 1280, width = img.width, height = img.height;

            if (width > height) {
                if (width > MAX_WIDTH) {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH;
                }
            } else {
                if (height > MAX_HEIGHT) {
                    width *= MAX_HEIGHT / height;
                    height = MAX_HEIGHT;
                }
            }
            canvas.width = width;
            canvas.height = height;
            ctx.drawImage(img, 0, 0, width, height);

            let dataUrl = canvas.toDataURL("image/jpeg");
            this.imgPreview.attr('src', dataUrl);
        };
    }

    scanning(file) {
        let data = new FormData();
        data.append('image', file);
        return fetch(this.endpoint_scaner, {
            method: 'post',
            body: data,
        }).then(res => {
            return res.json()
        })
    }

    handlerScanning(data) {
        this.modal.modal('hide');
        if ('valid' in data && !data.valid) {
            console.log('ERROR');
            setTimeout(()=>{
                this.modal.modal('hide');
            },1000)
        } else {
            this._insertScanningValue(data)
        }
    }

    _insertScanningValue(data) {
        this.first_name.val(data.names.split(' ')[0]);
        this.last_name.val(data.surname);

        if (data.valid_number) {

        }
        this.passport_number.val(data.number);

        if (data.valid_date_of_birth) {
            this.date_of_birth_picker.setDate(this.parseDate(data.date_of_birth, true))
        }

        if (data.valid_expiration_date) {
            this.passport_expiry_date_picker.setDate(this.parseDate(data.expiration_date))
        }

        let nationals = {'BLR': '11', 'POL': '112', 'UKR': '211'}, national = data.nationality;
        if (national in nationals) {
            this.nationality_id.val(nationals[national])
        }

        this.gender_id.val(data.sex === 'F' ? '2' : '1');
    }

    parseDate(date, check_down = false) {
        let pattern = /(\d{2})(\d{2})(\d{2})/,
            df = date.replace(pattern, '$1-$2-$3'),
            dp = df.split('-'),
            cy = parseInt(dp[0]);

        if (check_down) {
            cy = (cy > 18 ? '19' : '20') + cy;
        } else {
            cy = '20' + cy;
        }
        dp[0] = cy;
        let dt = new Date(dp.join('-')), d = dt.getDate(), m = dt.getMonth(), y = dt.getFullYear();
        let val = (d < 10 ? '0' + d : d) + '/' + (m + 1) + '/' + y;
        return flatpickr.parseDate(val, this.formatDate);
    }

}

const applicant = new Applicant();