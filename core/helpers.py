from django.utils.crypto import get_random_string, random
import string, random, requests, json


def make_random_password():
    length = random.randint(4, 11)
    lower = string.ascii_lowercase
    upper = string.ascii_uppercase
    digits = string.digits
    special = '$@#$!%*?&'
    chars = [lower, upper, digits, special]
    required = ''.join(random.choice(chars[i]) for i in range(4))
    random_password = get_random_string(length=length, allowed_chars=''.join(chars))
    return required + random_password


def make_random_phone():
    number = get_random_string(length=8, allowed_chars=string.digits)
    return '%d%s' % (random.randint(5, 7), number)


def make_random_applicants(results=20):
    response = requests.get(
        "https://randomuser.me/api/?inc=gender,name,nat,email&nat=GB,US&noinfo&results=%d" % results)
    results = json.loads(response.content)
    return results['results']



