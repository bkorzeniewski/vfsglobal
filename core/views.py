from django.contrib import auth, messages
from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from .forms import ApplicantForm, AppointmentForm
from .models import Applicant, Appointment, Account
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.validators import validate_image_file_extension
from django.core.exceptions import ValidationError
import json


def appointment_list(request, template_name='appointment/list.html'):
    appointment = Appointment.objects.filter(user=request.user)
    data = {}
    data['object_list'] = appointment
    data['title'] = 'Appointments'
    return render(request, template_name, data)


def appointment_create(request, template_name='appointment/create.html'):
    form = AppointmentForm(request.POST or None)
    if 'addanother' in request.POST:
        print(request.POST['addanother'])

    if form.is_valid():
        appointment = form.save(commit=False)
        appointment.user = request.user
        appointment.save()
        if 'addapplicant' in request.POST:
            return redirect('applicant_create', pk=appointment.pk)
        return redirect('appointment_list')
    return render(request, template_name, {'form': form, 'title': 'Add new appointment'})


def appointment_update(request, pk, template_name='appointment/update.html'):
    appointment = get_object_or_404(Appointment, pk=pk, user=request.user)
    form = AppointmentForm(request.POST or None, instance=appointment)
    if form.is_valid():
        form.save()
        return redirect('appointment_list')
    return render(request, template_name, {'form': form, 'title': ('Update appointment "%s"' % appointment)})


def appointment_delete(request, pk, template_name='appointment/delete.html'):
    appointment = get_object_or_404(Appointment, pk=pk, user=request.user)
    if request.method == 'POST':
        appointment.delete()
        return redirect('appointment_list')
    return render(request, template_name, {'object': appointment, 'title': 'Delete appointment'})


def applicant_list(request, pk, template_name='applicant/list.html'):
    appointment = get_object_or_404(Appointment, pk=pk, user=request.user)
    applicants = Applicant.objects.filter(appointment=appointment)
    data = {}
    data['object_list'] = applicants
    data['title'] = 'Applicants'
    data['appointment'] = appointment
    return render(request, template_name, data)


def applicant_create(request, pk, template_name='applicant/create.html'):
    appointment = get_object_or_404(Appointment, pk=pk, user=request.user)
    form = ApplicantForm(request.POST or None)
    if form.is_valid():
        applicant = form.save(commit=False)
        applicant.appointment = appointment
        applicant.account = Account.get_available()
        applicant.save()
        return redirect('applicant_list', pk=pk)
    return render(request, template_name, {'form': form, 'title': 'Add new applicant', 'appointment': appointment})


def applicant_update(request, pk, template_name='applicant/update.html'):
    applicant = get_object_or_404(Applicant, pk=pk)
    form = ApplicantForm(request.POST or None, instance=applicant)
    if form.is_valid():
        form.save()
        return redirect('applicant_list', pk=applicant.appointment.pk)
    return render(request, template_name, {'form': form, 'title': ('Update applicant "%s"' % applicant)})


def applicant_delete(request, pk, template_name='applicant/delete.html'):
    applicant = get_object_or_404(Applicant, pk=pk)
    if request.method == 'POST':
        applicant.delete()
        return redirect('applicant_list', pk=applicant.appointment.pk)
    return render(request, template_name, {'object': applicant, 'title': 'Delete applicant'})


def login(request, template_name='core/login.html'):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = auth.authenticate(username=username, password=password)

        if user is not None:
            # correct username and password login the user
            auth.login(request, user)
            return redirect('appointment_list')

        else:
            messages.error(request, 'Error wrong username/password')

    return render(request, template_name)


def dashboard(request):
    if request.user.is_authenticated:
        return redirect('appointment_list')
    return redirect('login')


def logout(request):
    auth.logout(request)
    return redirect('login')


@csrf_exempt
def upload(request):
    data = {}
    if request.method == 'POST':
        image_path = 'tmp/image.jpg'
        if 'image' in request.FILES:
            image = request.FILES['image']
            try:
                validate_image_file_extension(image)
                with open(image_path, 'wb+') as destination:
                    for chunk in image.chunks():
                        destination.write(chunk)
            except ValidationError:
                data = {'valid': False, 'error': 'invalid extension'}
        else:
            import base64
            image_base64 = request.POST.get('image')
            image_base64 = image_base64.replace('data:image/jpeg;base64,', '')
            imgdata = base64.b64decode(image_base64)
            with open(image_path, 'wb+') as fh:
                fh.write(imgdata)

        if not ('valid' in data and not data['valid']):
            from passporteye import read_mrz
            mrz = read_mrz(image_path)
            data = mrz.to_dict()
    return HttpResponse(json.dumps(data))


def handler404(request):
    response = render_to_response('core/404.html')
    response.status_code = 404
    return response
