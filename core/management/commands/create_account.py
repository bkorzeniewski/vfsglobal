from django.core.management.base import BaseCommand, CommandError
from core.models import Account
from core.helpers import make_random_applicants, make_random_phone, make_random_password


class Command(BaseCommand):
    help = 'Create Account'

    def add_arguments(self, parser):
        # parser.add_argument('id', nargs='+', type=int)
        pass

    def handle(self, *args, **options):
        # Account.objects.create()
        accounts = make_random_applicants(10)
        for account in accounts:
            name = account['name']
            email = account['email'].replace('example.com', 'extragol.pl')
            ap = Account(first_name=name['first'], last_name=name['last'], email_id=email,
                         contact_no=make_random_phone(), password=make_random_password())
            ap.save()
            print(ap)
        self.stdout.write(self.style.SUCCESS('Successfully'))
