from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model
from core.helpers import make_random_password


class Command(BaseCommand):
    help = 'Create Super User'

    def add_arguments(self, parser):
        parser.add_argument(
            '--username', dest='username', default=None,
            help='Specifies the username for the superuser.',
        )
        parser.add_argument(
            '--email', dest='email', default=None,
            help='Specifies the email for the superuser.',
        )
        parser.add_argument(
            '--password', dest='password', default=None,
            help='Specifies the password for the superuser.',
        )
        pass

    def handle(self, *args, **options):
        username = options.get('username')
        email = options.get('email')
        password = options.get('password')

        if not (username and email):
            raise CommandError('--username and --email are required')

        if not password:
            password = make_random_password()

        User = get_user_model()
        User.objects.create_superuser(username, email, password)
        output = 'username: %s \nemail: %s\npassword: %s' % (username, email, password)
        self.stdout.write(self.style.SUCCESS('Created new super user\n*****************'))
        self.stdout.write(output)
