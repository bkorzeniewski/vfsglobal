from django.forms import ModelForm
from .models import Applicant, Appointment
from django import forms


class DateInput(forms.DateInput):
    format = '%d/%m/%Y'


class DateField(forms.DateField):
    widget = forms.DateInput(format='%d/%m/%Y')
    input_formats = ['%d/%m/%Y', ]


class AppointmentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AppointmentForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

    expiry_date = DateField()

    class Meta:
        model = Appointment
        fields = ['location_id', 'visa_category_id', 'expiry_date']


class ApplicantForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ApplicantForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

    date_of_birth = DateField()
    passport_expiry_date = DateField()

    class Meta:
        model = Applicant
        fields = ['passport_number', 'nationality_id', 'date_of_birth', 'passport_expiry_date', 'first_name',
                  'last_name', 'gender_id', 'dial_code', 'phone_mobile', 'email_id']
        labels = {
            'nationality_id': 'Nationality',
            'gender_id': 'Gender',
            'email_id': 'Email'
        }
