from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
import datetime
from django.core.validators import RegexValidator

alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')


def validate_expiry_date(value):
    now = datetime.datetime.now().date()
    if value < now:
        raise ValidationError(_('Expiry date should be current or future date'))


def validate_passport_expiry_date(value):
    now = datetime.datetime.now().date()
    if value <= now:
        raise ValidationError(_('Passport expiry date should be future date'))


def validate_date_of_birth(value):
    now = datetime.datetime.now().date()
    if value >= now:
        raise ValidationError(_('Date of birth can\'t be current or future date'))
