from django.contrib import admin
from .models import Applicant, Appointment, Account
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from django.urls import reverse, path
from django.utils.safestring import mark_safe
from .helpers import make_random_applicants, make_random_phone, make_random_password


class ApplicantInline(admin.TabularInline):
    model = Applicant
    fields = ['last_name']


class AccountAdmin(admin.ModelAdmin):
    list_display = ['email_id', 'password', 'is_mail_confirmed', 'last_login', 'is_active', 'updated_at']
    list_editable = ('is_active',)
    list_filter = ('is_mail_confirmed', 'last_login', 'is_active')
    search_fields = ['email_id']

    def get_changeform_initial_data(self, request):
        account = make_random_applicants(1)[0]
        name = account['name']
        email = account['email'].replace('example.com', 'extragol.pl')
        return {'first_name': name['first'], 'last_name': name['last'], 'email_id': email,
                'contact_no': make_random_phone(), 'password': make_random_password()}

    class Media:
        css = {
            "all": ("core/css/admin.css",)
        }


class AppointmentAdmin(admin.ModelAdmin):
    list_display = ['location_name', 'visa_category_name', 'expiry_date', 'user_link', 'updated_at', 'created_at']
    list_filter = ('location_id', 'visa_category_id', 'expiry_date', 'user',)

    # list_display_links = ('location_name',)
    # search_fields = ['location_name']

    def user_link(self, obj):
        if obj.user is not None:
            user = obj.user
            url = reverse('admin:%s_%s_change' % (user._meta.app_label, user._meta.model_name), args=(user.pk,))
            return mark_safe('<a href="%s">%s</a>' % (url, user))


class AppointmentInline(admin.TabularInline):
    model = Appointment
    fields = ['location_id', 'visa_category_id', 'expiry_date', 'link']
    readonly_fields = ['link']
    extra = 0

    def link(self, obj):
        if obj.pk is not None:
            url = reverse('admin:%s_%s_change' % (obj._meta.app_label, obj._meta.model_name), args=(obj.pk,))
            return mark_safe('<a href="%s" class="button">Edit</a>' % url)
        else:
            return '-'


class MyUserAdmin(UserAdmin):
    inlines = [AppointmentInline]


class ApplicantAdmin(admin.ModelAdmin):
    pass


admin.site.register(Applicant)
admin.site.register(Appointment, AppointmentAdmin)
admin.site.register(Account, AccountAdmin)

admin.site.unregister(User)
admin.site.register(User, MyUserAdmin)
