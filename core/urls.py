"""vfsglobal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from core import views



urlpatterns = [
    path('appointment_list', views.appointment_list, name='appointment_list'),
    path('appointment_create', views.appointment_create, name='appointment_create'),
    path('appointment_edit/<int:pk>', views.appointment_update, name='appointment_edit'),
    path('appointment_delete/<int:pk>', views.appointment_delete, name='appointment_delete'),
    path('applicant_edit/<int:pk>', views.applicant_update, name='applicant_edit'),
    path('applicant_list/<int:pk>', views.applicant_list, name='applicant_list'),
    path('applicant_create/<int:pk>', views.applicant_create, name='applicant_create'),
    path('applicant_delete/<int:pk>', views.applicant_delete, name='applicant_delete'),

    path('', views.dashboard,  name='dashboard'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),

    path('upload', views.upload, name='upload'),

]
